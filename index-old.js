// @ts-nocheck
const o = (input, output = {...(input || {})}) => output;
const list = size => new Array(size || 0);
const object = (map, output = o()) => map.forEach(([key, value]) => output[key] = value) && output;
const names = (...names) => extend(names, object(names.map(name => [name, name])));
const enumm = (...keys) => extend(keys, object(keys.map(key => [key, Symbol(key)])));

const entries = obj => Object.entries(obj);
const keys = obj => Object.keys(obj);
const values = obj => Object.values(obj);
const extend = (...objects) => Object.assign(...objects);
const concat = (...objects) => extend({}, ...objects);
const copy = extend(item => {
	return Type.literal(item) ? item :
		Type.array(item) ? copy.array(item) : copy.object(item);
}, {
	object: obj =>
		map(obj, ([key, val]) => [key, copy(val)]),
	array: array =>
		array.map(copy),
});
const iterator = (obj, iterator, output, _map = entries(obj)) =>
	object(iterator(_map), output);
const map = (obj, mapper, output) =>
	iterator(obj, map => map.map(mapper), output);
const filter = (obj, filterer, output) =>
	iterator(obj, map => map.filter(filterer), output);
const apply = (obj, values) =>
	entries(values || {}).forEach(([key, val]) => obj[key] = val) || obj;
	
const sort = (obj, num_of_slots, sorter) =>
	entries(obj).reduce(
		(cubby, item, index, list) =>
			cubby[sorter(item, index, list)].push(item) && cubby,
		list(num_of_slots).fill(list())).map(map => object(map));

// 'x' is a type we are looking at. 'X' is another type. Are they the same, OR, does 'X' contain 'x'?
const satisfies = (x, X) => x === X || x.parents.some(x => satisfies(x, X));
/*
	- maths
		- some of that time stuff needs to go here too
		(maths.time.span? I kinda like it!!)
*/

const BUILT_IN_TYPES = names(
	'undefined',
	'symbol',
	'boolean',
	'number',
	'bigint',
	'string',
	'object',
	'function'
);
const META_TYPES = names(
	'type', // The TYPE type. A type that describes a type...
	'template', // The TEMPLATE type. The type that defines how to build types.
	'descriptor', // The DESCRIPTOR type. Describes template property configuration.
	'value', // A descriptor which describes static values
	'accessor', // A descriptor which describes variables (get-set)
	'procedure', // The PROCEDURE type. A template for building function types...
	'modifier', // The MODIFIER type. A procedure that builds DESCRIPTORS.
	'generator' // The GENERATOR type. A procedure that creates TEMPLATES.
);
const TYPE = Symbol("type");
const DO_NOTHING = (x => x);

const Type = {
	...object(BUILT_IN_TYPES.map(typename => [
		typename,
		val => typeof val === typename
	])),

	...object(META_TYPES.map(typename => [
		typename,
		val => val[TYPE] === typename
	])),
	
	literal: x => !Type.object(x) && !Type.function(x),
	true: val => val === true,
	integer: x => Type.number(x) && Number.isInteger(x),
	float: x => Type.number(x) && !Type.integer(x),

	is: (val, T) => val.constructor === T,
	raw: val => Type.is(val, Object),
	array: val => Type.is(val, Array),
	date: val => Type.is(val, Date),
};

// Yeah man... move it to some classes... hmm! Types built ONTOP of classes.
const DEFAULT_PROTOTYPE = {
	keys() {
		return keys(this);
	},
	values() {
		return values(this);
	},
	entries() {
		return entries(this);
	},
	map(mapper) {
		return map(this, mapper);
	},
	filter(filterer) {
		return filter(this, filterer);
	},
	sort(size, sorter) {
		return sort(this, size, sorter);
	},
	reduce(...args) {
		return entries(this).reduce(...reducer_args);
	},
	extend(...objects) {
		return extend(this, ...objects);
	},
	concat(...objects) {
		return concat(this, ...objects);
	},
	copy() {
		return copy(this);
	},
	apply(values) {
		return apply(this, values);
	},
	satisfies(type) {
		return satisfies(this[TYPE], type);
	},
};

const META_PROTOTYPE = {
	defines(instance) {
		return satisfies(instance[TYPE], this);
	}
};

const value = value => {
	return {
		value
	};
};

const getset = (id = "anonymous", get = DO_NOTHING, set = DO_NOTHING) => {
	return {
		get() {
			return get(this, id);
		},
		set(val) {
			return set(this, id, value);
		}
	};
};

// That's genuinely it...

const conversion_map = ([key, descr]) => {
	return [key, concat({
		configurable: false,
		writable: Type.true(descr.variable),
		enumerable: !Type.true(descr.private),
	}, descr.value ? value(descr.value) : getset(key, descr.get, descr.set))];
};

// First layer.
const assemble = {
	object: (props, proto = {}) => {
		return Object.create(
			concat(DEFAULT_PROTOTYPE, proto),
			map(props, conversion_map));
	},
	function: (get, props, proto) => {
		let obj = extend((...args) => {
			return func(obj, ...args);
		}, assemble(props), proto);
		let func = get(obj);
		return obj;
	},
	element: () => {
		//
	},
	constructor: (assembler, props, proto, applier) => {

	},
	raw: (values, proto = {}) => {
		return assemble.object(
			map(values, ([key, val]) => [key, value(val)]),
			proto);
	},
	variables: values => {
		return map(values, ([key, value]) => [key, {
			value,
			writable: true
		}]);
	},
	
	// wowza... that should be it!
	module: (mod, out = o()) =>
		map(mod, ([key, val]) =>
			[key, Type.generator(val) ? val(out, key) : val], out),
};

/* Right, now that we know TYPE goes in here, let's refactor... */
const construct = {
	object: (props, proto) => {
		return proto.constructor ?
			assemble.constructor(assemble.object, props, proto, (obj, ...args) => {
				proto.constructor.apply(obj, args);
				return obj;
			})
			function constructor(...args) {
				let obj = assemble.object(props, proto);
				proto.constructor.apply(obj, args);
				extend(obj, {
					[TYPE]: constructor,
				});
				return obj;
			} : function constructor(defaults = {}) {
				return apply(assemble.object(props, proto), {
					...defaults,
					[TYPE]: constructor
				})
			}
	},
	function: (props, proto) => {
		return proto.constructor ?
		// TODO: some sort of IN/OUT interface for arguments and return values. Keep'er simple.
		// This way we can wrap'er a bit and provide the appropriate type checking.
		// Perhaps arbitrary "validation" as well.
		(...args) => assemble.function(obj => proto.constructor.apply(obj, args), props, proto) :
			func => assemble.function(obj => func.bind(obj), props, proto);
	},
	element: () => {
		//
	},
};

const meta_props = () => {
	return {

	};
};

const meta_type = (type = null, id = 'anonymous', prototype = {}, properties = {}, parents = []) => {
	return {
		id,
		properties,
		parents,
		prototype,
		[TYPE]: type,
	};
};

const type = (id, construct_type, prototype = {}, properties = {}, static = {}, parents = []) => {
	/*
		Right, so in HERE is where we're gonna MERGE our shit with our parents' shit.
		Prolly the best way to do it.
	*/
	// But we only do that for the constructor. I think that best...
	let constructor = construct_type(properties, {
		...prototype
	});
	return extend(
		constructor,
		static,
		meta_type(constructor, id, prototype, properties, parents)
	);
};

extend(type, meta_type(
	type,
	META_TYPES.type,
	META_PROTOTYPE,
	assemble.variables(meta_type()) // TODO: update this with latest shit.
));

const type = assemble.function(
	(type,) => {
		
	}, {/* We don't define any properties here. */},
	{
		
	},
);

extend(type, type_properties(
	type,
	META_TYPES.type,
	{
		
	},
	assemble.variables(),
))

let temp = {
	[META_TYPES.type]: type(
		META_TYPES.type,
		construct.object,
		{

		}
	),

	[META_TYPES.template]: type(
		META_TYPES.template,
		construct.object,
		{

		}, {}, {},
		
	),
	
	"": type.object(META_TYPES.template, {
		constructor(id, construct, descriptor, ...parents) {
			let [literals, properties, prototype, static] = sort(descriptor, 4, ([key, val]) => {
				if (Type.literal(val))
					return 0;
				if (Type.modifier(val))
					return 1;
				return 2;
			});

			// ALSO! static properties.
			// THey can just be put in the descriptor... and they get merged with this object!

			// TODO: have I handled this appropriately?
			// ISSUE: this doesn't return a function...
			// See, before, we didn't have template as a type...
			// we can resolve this just need to think about it.
			// Right right this is where we are... let's make sure we get this bit right.
			
			return type(
				id,
				construct, {
					...object(concat(...parents.map(parent => parent.prototype))),
					...prototype
				}, {
					...object(concat(...parents.map(parent => parent.properties))),
					...properties,
				}, {
					...static,
					parents: parents,
					descriptor: descriptor,
					[TYPE]: type[META_TYPES.template],
				}
			);
		},
	}),

	[META_TYPES.procedure]: (id, descriptor, ...parents) =>
		extend(type.template(id, construct.function, descriptor, ...parents), {
			[TYPE]: META_TYPES.procedure
		}),

	[META_TYPES.generator]: type.procedure(META_TYPES.generator, {
		constructor() {
			return (module, id) => {
				// When we make a generator, this function is returned, yo.
			};
		}
	}),

	[META_TYPES.modifier]: type.procedure(META_TYPES.modifier, {
		constructor() {
			// We could put a hook into here...
			return (template, id) => {
				// This is the function returned when we create a modifier!
				// Yeah this hook-in is a bit better... what data do I need?

				// a value modifier...
				// Hey... descriptor and this modifier could be template?
				// We have it implemented below.....
				// hmmm!
			};
		}
	}),
	// listener... accessor.... variable.... etc.
	// they'll all be modifiers, but also have their sub-type represented if needed!!!

	// descriptor will be a PROPA template.
	[META_TYPES.descriptor]: type.object(META_TYPES.descriptor, {
		constructor(variable = false, private = false, required = false) {
			extend(this, {
				configurable: false,
				writable: variable,
				enumerable: !private,
				required: required,
			});
		},
		required() {
			this.required = true;
			return this;
		},
		private() {
			this.enumerable = false;
			return this;
		}
	}),
};

const generator_args = (...args) => [args.pop(), args];

const generator = (hook, ...args) => {
	let [descriptor, parents] = generator_args(args);
	return (module, id) => hook(id, descriptor, parents, module);
};

const func = (...args) => generator(() => {

});

const abstract = (...args) => {
	return (module, id) => {
		// This function is called by module.
		// this is actually making some sense. Wild.

	};
};

const model = (...args) => {
	let [descriptor, parents] = generator_args(args);
	return (module, id) => {

	};
};

/*

	z.module({
		...generators
	});

	// Goes through each key and, if the value is
	// a generator, it calls it as such...

*/
const module = description => {
	return {

	};
};
//

/*
	// An example of a function that creates a generator.
	z.element(
		z.ui.button,
		{
			id: z.attr.str().required(),
			active: z.attr.bool(false),
			connected: z.listener(...),
			...descriptor
		}
	);



	// This is a generator.

	// Called when a module is created and it returns a template.
	// yas yas yas.

	// THIS function should be wrapped in a GENERATOR call
	returns (module, name) => {
		// Yeah... I actually fuckin' like this.
		// All we gotta do is make sure the pipeline we provide
		// has some good hooks for us to exploit.
		return {
			...template
		}
	}
*/
const generator = script => {
	return () => {
		// Wraps our script with a tag that indicates that this is a generator
	}
};
//

/*

	z.attr.int(1, (el, key, int) =>
		console.log(`"${el.id}" ATTRIBUTE CHANGE @ "${key}": "${int}"`));
	
	// This function should be wrapped in a MODIFIERz call
	returns (template, key) => {
		return {
			...descriptor
		}
	}

*/
const modifier = script => {
	return () => {
		// Wraps our script with a "modifier" tag.
	}
};


// TODO: replace this with a module call...
let Z = module(z => {
	return {
		// The function to rule them all.
		create(definition, name = "anon") {
			// TODO: If we can inherit, these should set the default value
			// how did I do this before? It was a good trick so it was.
			// There was a way I did it... she'll come back to me...
			return build(
				literals.map(([key, value]) => [key, {
					// leave it like this?
					value: value
				}]).concat(descriptor),
				prototype
			);
		},
		define: {
			template() {
				// z.define.template => Allows us to define template generators...
			},
			property() {
				// z.define.property => Here we can create different sper-properties...
			},
		},
		module() {
			// This is how we create libraries and/or modules.
			// Fairly straightforward.
			// Uses what's been define(d) and what properties exist.
			// Loops through the map and uses the template and property definitiojns
			// to build the object that goes into z.create....
		},

		/*
			What about things like type, event, and error?
			Yeah... we can leave event and error out of it
			for the now.

			We'll focus on type.
			Everything that is created via a template is some
			sort of TYPE. Therefore, when digested by module(),
			it appends a reference to the type object
		*/
	}
});

// Might be some other shizz to do here.
module.exports = Z;
