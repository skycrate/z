# z

"zed" - an object type framework for JavaScript which allows for meta-programming and multiple inheritance.

## Quick Example

```js
	const examples = z.module({
		Message: z.model(),
		Listener: z.function({
			message: z.param('Message'),
		}),
		Channel: z.
		Dispatcher: z.abstract({
			channels: z.('Message').private()
			on(channel, listener) {
				
			},
			send(channel, message) {
				// ->
			}
		});
	});
```
